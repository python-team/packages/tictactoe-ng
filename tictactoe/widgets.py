#!/usr/bin/python

# tictactoe-widget.py
# (C) 2009 Alex Launi - alex.launi@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import math
import logging

import gtk
import glib
import cairo
import gobject
from gtk import gdk

import pygtk
pygtk.require('2.0')

import gettext

_ = gettext.lgettext

class TicTacToeBoard(gtk.DrawingArea):
    BAR_THICKNESS = 8
    SHAPE_THICKNESS = 25
    REPLAY_TIMEOUT = 750

    Blank = 0
    X = 1
    O = 2

    def __init__(self, parent=None):
        self.par = parent
        super(TicTacToeBoard, self).__init__()

        self.add_events(gdk.BUTTON_PRESS_MASK)
        self.connect('expose-event', self._expose)

        self._set_board()

    def new_game(self):
        self._set_board()
        self._expose(self, None)

    def make_move(self, space, player):
        x, y = space[0], space[1]

        if player == self.current_player and self.game_matrix[x][y] == self.Blank:
            self.move_count = self.move_count + 1
            self.game_matrix[x][y] = player

            self.winning_points = self.determine_if_winner(space)
            if self.move_count == 9 and not self.winning_points:
                self.winning_points = ((0,0), (0,0))
                self.winning_player = self.Blank
            else:
                self.winning_player = player

            if self.current_player == self.X:
                self.current_player = self.O
            else:
                self.current_player = self.X

        else:
            return False

        self._expose(self, None)

        return True

    def space_clicked(self, widget, event):
        (stage_w, stage_h) = widget.window.get_size()
        space = []

        if (event.y < stage_h * 1/3):
            space.append(0)
        elif (event.y < stage_h * 2/3):
            space.append(1)
        else:
            space.append(2)

        if (event.x < stage_w * 1/3):
            space.append(0)
        elif (event.x < stage_w * 2/3):
            space.append(1)
        else:
            space.append(2)

        return space

    def determine_if_winner(self, space):
        #check vertical
        if self.game_matrix[space[0]][0] == self.game_matrix[space[0]][1] == self.game_matrix[space[0]][2]:
            return ((space[0], 0), (space[0], 2))
        #check horizontal
        elif self.game_matrix[0][space[1]] == self.game_matrix[1][space[1]] == self.game_matrix[2][space[1]]:
            return ((0, space[1]), (2, space[1]))
        #check diagonal
        elif self.game_matrix[0][0] == self.game_matrix[1][1] == self.game_matrix[2][2] and self.game_matrix[0][0] != self.Blank:
            return ((0,0), (2,2))
        elif self.game_matrix[2][0] == self.game_matrix[1][1] == self.game_matrix[0][2] and self.game_matrix[2][0] != self.Blank:
            return ((2,0), (0,2))

    def _set_board(self):
        self.move_count = 0
        self.winning_points = None
        self.current_player = self.X
        self.game_matrix = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]

    def _expose(self, widget, event):
        cr = widget.window.cairo_create()

        #set white background
        cr.set_source_rgba(1.0, 1.0, 1.0, 1.0)
        cr.paint()

        self._draw_game_board(widget, cr)
        self._draw_players(widget, cr, self.game_matrix)
        if not self.winning_points is None:
            self._draw_cross_through(widget, cr)
            gobject.timeout_add(self.REPLAY_TIMEOUT, self._draw_replay_dialog, widget, cr)

        return False

    def _draw_game_board(self, widget, cr):
        (stage_w, stage_h) = widget.window.get_size()
        self._draw_bar(cr, 0, 0, stage_w, self.BAR_THICKNESS)
        self._draw_bar(cr, 0, stage_h * 1/3, stage_w, self.BAR_THICKNESS)
        self._draw_bar(cr, 0, stage_h * 2/3, stage_w, self.BAR_THICKNESS)
        self._draw_bar(cr, 0, 0, self.BAR_THICKNESS, stage_h)
        self._draw_bar(cr, stage_w * 1/3, 0, self.BAR_THICKNESS, stage_h)
        self._draw_bar(cr, stage_w * 2/3, 0, self.BAR_THICKNESS, stage_h)

    def _draw_bar(self, cr, x, y, width, height):
        '''Draws a gray semi-transparent bar for the space seperator'''
        cr.set_source_rgba(.9, 0.9, 0.9, 1.0)
        cr.rectangle(x, y, width, height)
        cr.fill()
        cr.stroke()

    def _draw_players(self, widget, cr, board):
        for i in range(len(board)):
            for j in range(len(board[i])):
                if board[i][j] == self.X:
                    self._draw_x(widget, cr, i, j)
                elif board[i][j] == self.O:
                    self._draw_o(widget, cr, i, j)

    def _draw_cross_through(self, widget, cr):
        #this is my stupid hack to handle the tie case better
        #this game has some serious design flaws...
        if self.winning_points[0] == self.winning_points[1]:
            return

        (stage_w, stage_h) = widget.window.get_size()

        cr.set_source_rgba(1.0, 0.0, 0.0, 1.0)
        cr.set_line_width(self.SHAPE_THICKNESS)

        startx_factor = 0
        starty_factor = 0
        endx_factor = 0
        endy_factor = 0
        #if our X winning points are equal, we've got a vertical win
        if self.winning_points[0][1] == self.winning_points[1][1]:
            startx_factor = 1.0/6 + self.winning_points[0][1] * 1.0/3
            endx_factor = startx_factor
            endy_factor = 1
        #if our Y winning points are equal we've got a horizontal win
        elif self.winning_points[0][0] == self.winning_points[1][0]:
            starty_factor = 1.0/6 + self.winning_points[0][0] * 1.0/3
            endy_factor = starty_factor
            endx_factor = 1
        #if we have winning points, then we've got a diagonal
        elif self.winning_points[0] == (0,0):
            startx_factor = 0
            starty_factor = 0
            endx_factor = 1
            endy_factor = 1
        elif self.winning_points[0] == (2,0):
            startx_factor = 0
            starty_factor = 1
            endx_factor = 1
            endy_factor = 0

        cr.move_to(stage_w * startx_factor + self.BAR_THICKNESS, self.BAR_THICKNESS + stage_h * starty_factor)
        cr.line_to(stage_w * endx_factor + self.BAR_THICKNESS, self.BAR_THICKNESS + stage_h * endy_factor)# * starty_factor)
        cr.stroke()

    def _draw_x(self, widget, cr, x, y):
        (stage_w, stage_h) = widget.window.get_size()

        cr.set_line_width(self.SHAPE_THICKNESS)
        cr.set_source_rgba(0, 1, 0, 0.8)
        xpos = stage_w * y/3 + self.BAR_THICKNESS/2 + self.SHAPE_THICKNESS
        ypos = stage_h * x/3 + self.BAR_THICKNESS/2 + self.SHAPE_THICKNESS
        cr.move_to(xpos, ypos)
        cr.line_to(xpos + stage_w/4, ypos + stage_h/4)
        cr.move_to(xpos, ypos + stage_w/4 - self.BAR_THICKNESS)
        cr.line_to(xpos + stage_w/4, ypos)
        cr.stroke()

    def _draw_o(self, widget, cr, x, y):
        (stage_w, stage_h) = widget.window.get_size()

        cr.set_line_width(self.SHAPE_THICKNESS)
        cr.set_source_rgba(1.0, 0.5, 0.31, 1.0)

        xpos = stage_w * y/3 + stage_w/7 + self.BAR_THICKNESS/2 + self.SHAPE_THICKNESS/2
        ypos = stage_h * x/3 + stage_h/7 + self.BAR_THICKNESS/2 + self.SHAPE_THICKNESS/2

        cr.save()
        cr.translate(xpos, ypos)
        cr.arc(0.0, 0.0, stage_w/8, 0, 2*math.pi)
        cr.stroke()
        cr.restore()
    def _draw_replay_dialog(self, widget, cr):
        (stage_w, stage_h) = widget.window.get_size()

        xpos = stage_w * 1/6 + self.BAR_THICKNESS
        ypos = stage_h * 1/3 - self.BAR_THICKNESS * 2
        width = stage_w * 2/3
        height = stage_h * 1/3

        self._draw_board_screen(widget, cr)
        self._write_winner_loser(widget, cr, self.BAR_THICKNESS, stage_h * 1/3 - self.SHAPE_THICKNESS)
        self._draw_rounded_rectangle(widget, cr, xpos, ypos, width, height, 40)
        self._write_replay_text(widget, cr, xpos + width/4, ypos + height/2 + self.BAR_THICKNESS)

        return False

    def _draw_board_screen(self, widget, cr):
        (stage_w, stage_h) = widget.window.get_size()
        cr.set_source_rgba(0.3, 0.3, 0.3, 0.8)
        cr.rectangle(0, 0,  stage_w, stage_h)
        cr.fill()
        cr.stroke()

    def _draw_rounded_rectangle(self, widget, cr, x, y, width, height, radius):
        if width/2 < radius or height/2 < radius:
            radius = min(width/2, height/2)

        cr.save()
        cr.set_source_rgba(0.96, 0.96, 0.96, 0.75)
        cr.move_to(x, y + radius)
        cr.arc(x + radius, y + radius, radius, math.pi, -math.pi/2)
        cr.line_to(x + width - radius, y)
        cr.arc(x + width - radius, y + radius, radius, -math.pi/2, 0)
        cr.line_to(x + width, y + height - radius)
        cr.arc(x + width - radius, y + height - radius, radius, 0, math.pi/2)
        cr.line_to(x + radius, y + height)
        cr.arc(x + radius, y + height - radius, radius, math.pi/2, math.pi)
        cr.close_path()
        cr.fill()
        cr.restore()

        return False


    def _write_winner_loser(self, widget, cr, x, y):
        if self.winning_player == self.X:
            text = _('Cross wins')
        elif self.winning_player == self.O:
            text = _('Circle wins')
        else:
            text = _('It\'s a tie')

        self._write(widget, cr, x, y, 75, text)

    def _write_replay_text(self, widget, cr, x, y):
        #cr.set_source_rgb(1.0, 1.0, 1.0)
        #cr.select_font_face("sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
        #cr.set_font_size(50)
        #cr.move_to(x, y)
        self._write(widget, cr, x, y, 50, _('Again?'))

    def _write(self, widget, cr, x, y, size, text):
        cr.set_source_rgb(1.0, 1.0, 1.0)
        cr.select_font_face("sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
        cr.set_font_size(size)
        cr.move_to(x, y)
        cr.show_text(text)

class WaitingAnimation(gtk.DrawingArea):
    TIMEOUT = 100
    NUM_LINES = 8
    BAR_THICKNESS = 20

    def __init__(self, parent=None):
        self.par = parent
        super(WaitingAnimation, self).__init__()

        self.count = 0

        self.trs = (
            ( 0.0, 0.15, 0.30, 0.5, 0.65, 0.80, 0.9, 1.0 ),
            ( 1.0, 0.0,  0.15, 0.30, 0.5, 0.65, 0.8, 0.9 ),
            ( 0.9, 1.0,  0.0,  0.15, 0.3, 0.5, 0.65, 0.8 ),
            ( 0.8, 0.9,  1.0,  0.0,  0.15, 0.3, 0.5, 0.65 ),
            ( 0.65, 0.8, 0.9,  1.0,  0.0,  0.15, 0.3, 0.5 ),
            ( 0.5, 0.65, 0.8, 0.9, 1.0,  0.0,  0.15, 0.3 ),
            ( 0.3, 0.5, 0.65, 0.8, 0.9, 1.0,  0.0,  0.15 ),
            ( 0.15, 0.3, 0.5, 0.65, 0.8, 0.9, 1.0,  0.0, )
        )

        self.connect('expose-event', self._expose)

    def start(self):
        glib.timeout_add(self.TIMEOUT, self.on_timer)

    def on_timer(self):
        self.count = self.count + 1
        self.queue_draw()
        return True

    def _expose(self, widget, event):
        cr = widget.window.cairo_create()

        #set white background
        cr.set_source_rgba(1.0, 1.0, 1.0, 1.0)
        cr.paint()

        cr.set_line_width(self.BAR_THICKNESS)
        cr.set_line_cap(cairo.LINE_CAP_ROUND)

        w = self.allocation.width
        h = self.allocation.height

        cr.translate(w/2, h/2)

        for i in range(self.NUM_LINES):
            cr.set_source_rgba(0, 0, 0, self.trs[self.count%8][i])
            cr.move_to(0.0, -35.0)
            cr.line_to(0.0, -100.0)
            cr.rotate(math.pi/4)
            cr.stroke()

class WelcomePane(gtk.VBox):
    def __init__(self, parent=None):
        self.par = parent
        super(WelcomePane, self).__init__()

        self.connect('expose-event', self._expose)

        self._option_table = gtk.Table(1, 2, False)
        self._option_table.set_col_spacings(12)
        self._option_table.set_homogeneous(False)

        header_table = gtk.Table(1, 2, False)
        header_table.set_col_spacings(12)

        header_icon = gtk.image_new_from_icon_name('face-laugh', gtk.ICON_SIZE_DIALOG)
        header_icon.yalign = 0.0
        header = gtk.Label()
        header.set_markup(_('<span size=\"xx-large\"><b>How would you like to play today?</b></span>'))
        header.set_alignment(0.0, 0.5)

        header_table.attach(header_icon, 0, 1, 0, 1, gtk.FILL, gtk.FILL, 0, 0)
        header_table.attach(header, 1, 2, 0, 1, gtk.EXPAND | gtk.FILL, gtk.FILL, 0, 0)

        self._header_align = gtk.Alignment(0, 1, 0, 0)
        self._table_align = gtk.Alignment(0, 0, 0, 0)
        self._set_alignments()

        self._header_align.add(header_table)

        self.pack_start(self._header_align, False, False, 5)

        self._row = 0

        self._table_align.add(self._option_table)
        self.pack_start(self._table_align, False, False, 0)

        self.show_all()

    def _set_alignments(self):
        header_top_spacing = (self.par.get_size()[0] - 430) / 2
        self._header_align.set_padding(25, 5, header_top_spacing, 0)
        self._table_align.set_padding(15, 0, header_top_spacing + 20, 0)

    def _attach_arrow(self, row):
        arrow_icon = gtk.image_new_from_stock(gtk.STOCK_GO_FORWARD, gtk.ICON_SIZE_BUTTON)
        arrow_icon.set_alignment(0.0, 1.0)
        arrow_icon.show()
        self._option_table.attach(arrow_icon, 0, 1, self._row, self._row + 1, gtk.FILL, gtk.FILL, 0, 0)

    def append(self, choice, detail, callback):
        button = gtk.Button()
        button.set_label(_('Play a game of tic tac toe against %s') % choice)
        if callback is not None:
            button.connect('clicked', callback)

        detail_label = self._build_detail_label('<span size=\"small\" color=\"#36454F\">%s</span>' % detail)
        detail_label.show()

        self._attach_arrow(self._row)

        self._option_table.attach(button, 1, 2, self._row, self._row + 1, gtk.EXPAND | gtk.FILL, 0, 0, 0)
        self._option_table.attach(detail_label, 1, 2, self._row + 1, self._row +  2, gtk.EXPAND | gtk.FILL, 0, 0, 0)
        self._option_table.set_row_spacing(self._row + 1, 15)

        self._row = self._row + 2

    def _build_detail_label(self, markup):
        label = gtk.Label()
        label.set_markup(markup)
        label.set_alignment(0.0, 0.5)
        label.set_line_wrap(True)
        return label

    def _expose(self, widget, event):
        self._set_alignments()
        cr = widget.window.cairo_create()
        cr.set_source_rgba(1.0, 1.0, 1.0)
        cr.paint()
