#!/usr/bin/python

# tubehandler.py
# (C) 2009 Alex Launi - alex.launi@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import logging
import os

import dbus
import dbus.service
import gtk
from dbus import PROPERTIES_IFACE
from dbus.mainloop.glib import DBusGMainLoop
from gettext import gettext as _

from telepathy.client.conn import Connection
from telepathy.client.channel import Channel
from telepathy.constants import (
        TUBE_STATE_OPEN,
        SOCKET_ACCESS_CONTROL_CREDENTIALS)
from telepathy.interfaces import (
        CHANNEL_INTERFACE,
        CHANNEL_INTERFACE_TUBE,
        CHANNEL_TYPE_DBUS_TUBE,
        CONN_INTERFACE_ALIASING)

import gettext

_ = gettext.lgettext
logger = logging.root

DBusGMainLoop(set_as_default=True)

def _get_contact_alias(conn, handle):
    return conn[CONN_INTERFACE_ALIASING].RequestAliases([handle])[0]

def _sanitize_service_name(service_name):
    return service_name.replace('.', '_')

def _build_bus_name(service_name):
    return "org.gnome.Empathy.DTubeHandler.%s" % _sanitize_service_name(service_name)

def _build_object_path(bus_name):
    return '/' + bus_name.replace('.', '/')

class TubeHandler(dbus.service.Object):
    """Listen to coming tube

    tube_received_cb is called when the tube becomes open
    """

    def __init__(self, service_name, tube_received_cb):
        logger.debug('creating TubeHandler')
        self._tube_received_cb = tube_received_cb

        bus_name = _build_bus_name(service_name)
        obj_path = _build_object_path(bus_name)
        logger.debug('Bus name: %s' % bus_name)
        logger.debug('Object path: %s' % obj_path)

        bus = dbus.SessionBus()
        # this bus_name has to be retained, so make it a instance attribute
        self.busname = dbus.service.BusName(bus_name, bus)
        super(TubeHandler, self).__init__(bus, obj_path)

    @dbus.service.method(dbus_interface="org.gnome.Empathy.TubeHandler", in_signature='soouu', out_signature='')
    def HandleTube(self, bus_name, connection, channel, handle_type, handle):
        """Called when we are offered a tube"""
        logger.debug('Tube received on %s' % channel)
        self._bus_name = bus_name
        self._channel = channel
        self._peer_handle = handle
        self._conn = Connection(bus_name, connection, ready_handler=self._ready_handler)

    def _ready_handler(self, conn):
        self._tube_chan = Channel(self._bus_name, self._channel)
        self._tube_chan[CHANNEL_INTERFACE_TUBE].connect_to_signal(
            'TubeChannelStateChanged', self._tube_state_changed_cb)

        params = self._tube_chan[PROPERTIES_IFACE].Get(
            CHANNEL_INTERFACE_TUBE, 'Parameters')
        self._process_incoming_tube(params)

    def _tube_state_changed_cb(self, state):
        if state == TUBE_STATE_OPEN:
            logger.debug('Tube state changed ->open')
            bus = dbus.connection.Connection(self._address)
            self._tube_received_cb(bus, self._tube_chan)
        else:
            logger.debug('Tube state changed ->%s' % state)

    def _process_incoming_tube(self, params):
        def reply_cb(dialog, response_id):
            if response_id == gtk.RESPONSE_ACCEPT:
                self._address = self._tube_chan[CHANNEL_TYPE_DBUS_TUBE].Accept(
                    SOCKET_ACCESS_CONTROL_CREDENTIALS)
            else:
                self._tube_chan[CHANNEL_INTERFACE].Close()
            dialog.destroy()

        alias = _get_contact_alias(self._conn, self._peer_handle)
        logger.debug('Processing incoming tube request')

        dialog = gtk.Dialog(_('New game request!'), None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                     (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                      gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        dialog.connect('response', reply_cb)

        label = gtk.Label()
        label.set_line_wrap(True)
        label.set_markup(_('<big><b>%s would like to play a game of tic tac toe with you! Would you like to play?</b></big>') % alias)
        label.show()

        align = gtk.Alignment(1, 1, 0, 0)
        align.set_padding(5, 5, 5, 5)
        align.add(label)

        vbox = dialog.get_content_area()
        vbox.pack_start(align, True, True, 0)
        vbox.show_all()

        dialog.run()
