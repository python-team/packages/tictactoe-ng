#!/usr/bin/python

# main.py
# (C) 2009 Alex Launi - alex.launi@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import sys
import logging
import random
import time

import gtk

from widgets import TicTacToeBoard
from widgets import WaitingAnimation
from widgets import WelcomePane

import tptube
import tubehandler
import contactselector

import networking

# i18n
import locale
import gettext

LOCALEDIR = '/usr/share/locale'
DOMAIN = 'tictactoe'

locale.setlocale(locale.LC_ALL, "")
gettext.bindtextdomain(DOMAIN, LOCALEDIR)
gettext.textdomain(DOMAIN)

_ = gettext.lgettext
logger = logging.root

class TicTacToeGame(gtk.Window):

    _single_title = _('tic-tac-toe all by yourself')
    _tube_title = _('tic-tac-toe with %s')

    _tube_service = 'org.gnome.TicTacToe'
    _initiator_obj_path = '/Initiator'
    _receiver_obj_path = '/Receiver'

    def __init__(self):
        super(TicTacToeGame, self).__init__()

        random.seed(time.time())
        try:
            self.set_icon_name("tictactoe")
        except:
            logger.warning('No tictactoe icon exists; not using window icon')

        self.set_default_size(600, 600)
        self.set_geometry_hints(self, min_width=500, min_height=500, max_width=-1, max_height=-1, min_aspect=1.0, max_aspect=1.0)
        #self.set_size_request(500, 500)
        self.set_app_paintable(True)
        self.set_title(self._single_title)
        self.set_position(gtk.WIN_POS_CENTER)

        self._content_vbox = gtk.VBox(False, 0)

        self._board = TicTacToeBoard(self._content_vbox)
        self.on_button_press_handler = self._board.connect('button-press-event', self.on_board_button_press_event)
        self._build_menu()

        self._welcome = WelcomePane(self)
        self._welcome.append(_('the computer'), _('Bored? All alone? Play by yourself vs. the computer, but watch out the computer might be smarter than you think!'), self._new_with_computer_clicked)
        self._welcome.append(_('an online friend'), _('You can even play instantly, online, right now, at this very moment, with one of your Empathy contacts.'), self._new_with_contact_clicked)
        self._welcome.append(_('a friend sitting next to you'), _('Hanging out with a friend? Want to play tic tac toe but are totally into this green zero-paper lifestyle craze? Trade the mouse back and forth in this fast paced high aventure game of tic tac toe.'), self._new_with_local_friend_clicked)

        self._content_vbox.pack_start(self._welcome, True, True, 0)
        #self._content_vbox.pack_start(self._board, True, True, 0)

        self.add(self._content_vbox)
        self.connect('destroy', gtk.main_quit)

        self.in_game = False
        self.player = self._board.X
        self.computer = None
        self._friend = None

        self._tube = None
        self._peer = None
        self._is_initiator = None
        self._is_waiting_for_response = None # this should be extraced out

        self.tubehandler = tubehandler.TubeHandler(self._tube_service, tube_received_cb=self._process_accepted_tube)

        self.show_all()

        # this is kind of silly but we call this here to build the avatar cache
        contactselector.ContactSelector()

    def _playable(self):
        return self.in_game and self._board.winning_points is None and self._board.move_count < 9

    def _start_game(self):
        def reply_handler(*args):
            logger.debug('signal peer: %s' % str(args))
        def error_handler(error):
            logger.debug('signal peer: %s' % error)

        self.in_game = True
        self._board.new_game()

        if self._peer:
            self._peer.new_game(
                reply_handler=reply_handler,
                error_handler=error_handler)

        self.queue_draw()

    def _end_game(self):
        self.in_game = False
        self._board.disconnect(self.on_button_press_handler)
        self.on_button_press_handler = self._board.connect('button-press-event', self._on_board_newgame_button_press)

    def _build_menu(self):
        accel_group = gtk.AccelGroup()
        self.add_accel_group(accel_group)

        menubar = gtk.MenuBar()
        gamemenu = gtk.Menu()
        gamem = gtk.MenuItem(_('_Game'))
        gamem.set_submenu(gamemenu)

        new_game_item = gtk.MenuItem(_('_New game'))
        new_game_item.connect('activate', self._on_newgame_menu_btn_clicked)
        gamemenu.append(new_game_item)

        sep = gtk.SeparatorMenuItem()
        gamemenu.append(sep)

        exit_item = gtk.ImageMenuItem(gtk.STOCK_QUIT, accel_group)
        key, mod = gtk.accelerator_parse('Q')
        exit_item.add_accelerator('activate', accel_group, key,
            mod, gtk.ACCEL_VISIBLE)
        exit_item.connect('activate', gtk.main_quit)

        gamemenu.append(exit_item)

        menubar.append(gamem)
        self._content_vbox.pack_start(menubar, False, False, 0)

    def _get_computer_move(self):
        found_winner = False
        winner = [0, self.computer, self.computer]
        loser = [0, self.player, self.player]
        board = self._board.game_matrix
        # in case there aren't any other decent moves we just use (2, 2) or a random
        if board[1][1] == self._board.Blank:
            x = y = 1
        else:
            x = random.randint(0, 2)
            y = random.randint(0, 2)

        # find any horizontal wins
        i = 0
        for row in board:
            tmp = list(row)
            tmp.sort()

            if tmp == winner or tmp == loser:
                x = i
                y = row.index(self._board.Blank)
                if tmp == winner:
                    found_winner = True
                    break
            i = i + 1

        for i in range(3):
            if found_winner:
                break
            col = []
            for j in range(3):
                col.append(board[j][i])
            tmp = list(col)
            tmp.sort()
            if tmp == winner or tmp == loser:
                x = col.index(self._board.Blank)
                y = i
                if tmp == winner:
                    found_winner = True
                    break

        diag1 = [board[0][0], board[1][1], board[2][2]]
        diag2 = [board[2][0], board[1][1], board[0][2]]
        tmp1 = list(diag1)
        tmp1.sort()
        tmp2 = list(diag2)
        tmp2.sort()

        if not found_winner:
            diag = None
            if tmp1 == winner:
                x = y = diag1.index(self._board.Blank)
            elif tmp2 == winner:
                y = diag2.index(self._board.Blank)
                if y == 0:
                    x = 2
                elif y == 1:
                    x = 1
                else:
                    x = 0
            elif tmp1 == loser:
                x = y = diag1.index(self._board.Blank)
            elif tmp2 == loser:
                y = diag2.index(self._board.Blank)
                if y == 0:
                    x = 2
                elif y == 1:
                    x = 1
                else:
                    x = 0

        return (x, y)

    def _on_newgame_menu_btn_clicked(self, action):
        self._set_main_widget(self._welcome)

    def _new_with_computer_clicked(self, action):
        self.computer = self._board.O
        self._show_board()
        self._start_game()

    def _new_with_local_friend_clicked(self, action):
        self._show_board()
        self._start_game()

    def _new_with_contact_clicked(self, action):
        selector = contactselector.ContactSelector()
        selector.connect('item-activated', self._on_contact_activated)

        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_NONE)
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        sw.add(selector)

        self._set_main_widget(sw, True)

    def on_board_button_press_event(self, widget, event):
        def reply_handler(*args):
            logger.debug('signal peer: %s' % str(args))
        def error_handler(error):
            logger.debug('signal peer: %s' % error)

        if not self._playable():
            self.in_game = True
            return

        space = self._board.space_clicked(widget, event)

        if self._board.make_move(space, self.player):
            if self._peer:
                self._peer.make_move(space[0], space[1], self.player, reply_handler=reply_handler, error_handler=error_handler)
            elif self.computer is not None:
                while self._playable():
                    space = self._get_computer_move()
                    if self._board.make_move(space, self.computer):
                        break
            else:
                if self.player == self._board.X:
                    self.player = self._board.O
                else:
                    self.player = self._board.X

        if not self._playable():
            self._end_game()

        return False

    def _on_board_newgame_button_press(self, widget, event):
        self._board.disconnect(self.on_button_press_handler)
        self._start_game()
        self.on_button_press_handler = self._board.connect('button-press-event', self.on_board_button_press_event)

        if not self._peer:
            self.player = self._board.X

        self.in_game = True

    def _on_contact_activated(self, widget, iter):
        def response_cb(dialog, response_id):
            if self._is_waiting_for_response:
                if response_id in (gtk.RESPONSE_CANCEL,
                    gtk.RESPONSE_DELETE_EVENT):
                    self._tube_offer.cancel()
                    self._is_initiator = None
            # else don't need to do anything
            self._show_board()

        def offer_fail_cb(offer, reason):
            self._is_waiting_for_response = False
            self._is_initiator = None

            self._set_main_widget(self._welcome)
            logger.error('failed to connect dbus tube')

        model = widget.get_model()
        contact = model[iter]
        if contact:
            conn, handle, contact, alias, icon = contact
            choice = (conn, handle)

        if not choice:
            return

        self._friend = alias
        self.in_game = True
        self._is_initiator = True
        self.player = self._board.X
        conn, handle = choice

        self._tube_offer = tptube.TubeOffer(conn, handle, self._tube_service)
        self._tube_offer.connect("offer-succeeded", self._tube_offered_cb)
        self._tube_offer.connect("offer-failed", offer_fail_cb)

        self._waiting_animation()

        try:
            self._tube_offer.execute()
        except:
            offer_fail_cb(self._tube_offer, "You can't tube yourself, weirdo")

    def _set_main_widget(self, widget, cancelable=False, stock=gtk.STOCK_GO_BACK, callback=None):
        self._previous = self._content_vbox.get_children()[1]
        self._content_vbox.remove(self._previous)

        if callback is None:
            callback = self._cancel_btn_clicked

        if cancelable:
            cancel_btn = gtk.Button(stock=stock)
            cancel_btn.connect('clicked', callback)

            bbox = gtk.HButtonBox()
            bbox.set_layout(gtk.BUTTONBOX_END)
            bbox.pack_start(cancel_btn, False, False, 5)

            halign = gtk.Alignment(1, 0, 0, 0)
            halign.set_padding(2, 0, 0, 3)
            halign.add(bbox)

            vbox = gtk.VBox()
            vbox.set_spacing(2)
            vbox.pack_start(widget, True, True, 0)
            vbox.pack_end(halign, False, False, 0)
            self._content_vbox.add(vbox)
        else:
            self._content_vbox.add(widget)

        self._content_vbox.show_all()

    def _set_playing_with_friend_title(self):
        if self._friend:
            title = self._tube_title % self._friend
        else:
            title = self._tube_title % _("a friend")
        self.set_title(title)

    def _board_showing(self):
        return self._content_vbox.get_children()[1] == self._board

    def _show_board(self):
        if self._board_showing():
            return
        self.in_game = True
        self._set_main_widget(self._board)


    def _waiting_animation(self):
        waiting = WaitingAnimation(self._content_vbox)
        self._set_main_widget(waiting)
        waiting.start()

    def _cancel_btn_clicked(self, button):
        self._set_main_widget(self._previous)

    def _tube_offered_cb(self, offer, bus, tube):
        """Called when the peer accepts our tube"""
        self._show_board()
        self._board.new_game()
        self._tube = tube

        self._tube[tptube.CHANNEL_INTERFACE].connect_to_signal(
            'Closed', self._tube_closed_cb)

        if not self._negotiate_peer_proxy(bus):
            return

        self._set_playing_with_friend_title()

    def _tube_closed_cb(self):
        self._is_initiator = None
        self._tube = None
        self._friend = None

    def _process_accepted_tube(self, bus, tube):
        """User has accepted the tube
        """

        self._is_initiator = False
        self._tube = tube
        self._tube[tptube.CHANNEL_INTERFACE].connect_to_signal(
            'Closed', self._tube_closed_cb)

        if not self._negotiate_peer_proxy(bus):
            return

        self._set_playing_with_friend_title()
        self.player = self._board.O
        self._show_board()
        self._board.new_game()

    def _incoming_cancel_btn_clicked(self, action, reply_handler):
        self._content_vbox.remove(self._content_vbox.get_children()[1])
        reply_handler(gtk.RESPONSE_CANCEL)

    def _negotiate_peer_proxy(self, bus):
        if self._is_initiator:
            me, peer = self._initiator_obj_path, self._receiver_obj_path
        else:
            me, peer = self._receiver_obj_path, self._initiator_obj_path

        myself = networking.Peer(bus, me, self._board)
        try:
            self._peer = bus.get_object(object_path=peer)
        except dbus.exceptions.DBusException, err:
            logger.error('Error when contacting with initiator: %s' % err)
            return False
        return True

if __name__ == '__main__':
    TicTacToeGame()
    gtk.main()
